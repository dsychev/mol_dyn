#include <stdio.h>
#include <stdlib.h>
#include<math.h>
#include<string.h>

//���������� ���������� �������
const double distance = 1, e = 1, mass_of_part = 1, k_boltz = 1; 	// k_boltz = 1.38E-23 J/K mass_of_elec = 9.1E-31 kg
double alpha = 5E-3;  									//alpha = 23E-29 (su), e = 1.6E-19 C  distance =  3E-7 m
double momentum_0 = 0.001;											// momentum_avarage = sqrt(2*k_boltz*mass_of_elec*T) = 3.54E-11 m*kg/s
const int amount_of_par = 100;

//���������� ��� ����������
const int N_of_tau = 10000;
const double tau = 0.001;

//������� ��� ������� ��������� ���������
void initial_coord(double [amount_of_par][2], const int , const double , double []);
void initial_deltX(double [amount_of_par][2]);
void initial_deltP(double [amount_of_par][2], double);
static inline double match_within_R_cut(double, double, double);

//�������������� �������
void calculations(double [2][amount_of_par][2], double [2][amount_of_par][2], const int ,
					const double [], const double , const int , const int , const int , char *,
					const int );
void first_stage(double [2][amount_of_par][2], double [2][amount_of_par][2], double [amount_of_par][2],
					const int ,	double [], const double , const double ,
					const double [], const double ,  double [], const double , double *, const int ,
					const int , const double , FILE *, const int);
void cor_func(double [][2], const int , double [], const int ,
				const double , const double, const double );

//������� ������
void print_result_coord_vert(const double [amount_of_par][2]);



int main(void) {

	double coordinats[2][amount_of_par][2], moment[2][amount_of_par][2], per_s[2];
	char name[40];
	strcpy(name,"megafile");

	//������� ��������� �������
	initial_coord(coordinats[0],amount_of_par,distance,per_s);
	initial_deltX(coordinats[0]);
	initial_deltP(moment[0], momentum_0);
	printf("Initial coordinats (x,y):\n");
	print_result_coord_vert(coordinats[0]);
	printf("Periods of the system:\n%-10f%-10f\n\n",per_s[0], per_s[1]);

	//�������������� �������, ��������� ��� � megafile
	calculations(coordinats,moment,amount_of_par,per_s,act_rad,N_of_tau,2000,1000,name,100);

	return EXIT_SUCCESS;
}


void calculations(double coordin[2][amount_of_par][2], double moment[2][amount_of_par][2], const int num_par,
					const double per_sys[], const double radius, const int N_of_it, const int start, const int N, char *name,
					const int per){
	int i = 0, a = 0;
	double f[num_par][2], distr[N], h_val = 0;
	double temper[1];
	double mega_array[2], l = 0, dr = 0;
	char st[20], st1[20], st2[40];
	char *st3 = st2;
	FILE *sfPtr;
	temper[0] = 0;

	l = fmin(per_sys[0],per_sys[1])/2;
	dr = l/N;
	for(i = 0; i < N; i++){
		distr[i] = 0;
	}
	sprintf(st,".txt");

	for(i = 0; i < N_of_it; i++){
		a = i%per;
		if(a == 0){
			sprintf(st1,"_%d",i);
			sprintf(st3,"%s",name);
			strcat(st3,st1);
			strcat(st3,st);
			sfPtr = fopen(st3,"w");
		}
		first_stage(coordin,moment,f,num_par,mega_array,mass_of_part,alpha,per_sys,l,distr,dr,temper,start,i,tau,sfPtr,a);
		fclose(sfPtr);
	}
	temper[0] /= (N_of_it - start);
	printf("Temperature = %-10f\n",temper[0]);
	h_val = M_PI*dr*dr*pow(num_par,2)*(N_of_it - start);
	for(i = 0; i < N; i++){
		distr[i] /= h_val;
		printf("%-10f%-25.15f\n", ((i+1)*dr/2), distr[i]);
	}
}

void first_stage(double coordin[2][amount_of_par][2], double moment[2][amount_of_par][2], double f[amount_of_par][2],
					const int num_par,		double mega_array[2], const double m, const double alpha_1,
					const double per_x_y[], const double l,  double distr[], const double dr, double *T, const int start,
					const int n_iter, const double tau, FILE *fPtr, const int a){

	int i = 0, j = 0, k = 0;
	double d = 0, pot_en = 0, kin_en = 0;
	for(i = 0; i < num_par; i++){
		f[i][0] = 0;
		f[i][1]	= 0;
		for(j = 0; j < num_par; j++){
			mega_array[0] =  match_within_R_cut((coordin[0][i][0] - coordin[0][j][0]),l, per_x_y[0]);
			mega_array[1] =  match_within_R_cut((coordin[0][i][1] - coordin[0][j][1]),l, per_x_y[1]);
			if( i != j){
				if( (fabs(mega_array[0]) <= l) & (fabs(mega_array[1]) <= l) ){
					d = sqrt(pow(mega_array[0],2) + pow(mega_array[1],2));
					if( d <= l ){
						f[i][0] += mega_array[0]/(d*d*d);
						f[i][1]	+= 	mega_array[1]/(d*d*d);
						if( n_iter > start){
							k = floor(d/dr);
							distr[k] += 1/(pow(k+1,2) - pow(k,2));
						}
						if(j < i){
							pot_en += 1/d;
						}
					}
				}
			}
		}
		f[i][0] *= alpha_1;
		f[i][1]	*= alpha_1;
		if( n_iter == 0 ){
			moment[1][i][0] = moment[0][i][0] + tau*f[i][0]/2;
			moment[1][i][1] = moment[0][i][1] + tau*f[i][1]/2;
		} else {
			moment[1][i][0] = moment[0][i][0] + tau*f[i][0];
			moment[1][i][1] = moment[0][i][1] + tau*f[i][1];
		}
		coordin[1][i][0] = coordin[0][i][0] + tau*moment[1][i][0];
		coordin[1][i][1] = coordin[0][i][1] + tau*moment[1][i][1];
		if( a == 0){
			fprintf(fPtr,"%-15f%-20f\n",coordin[1][i][0],coordin[1][i][1]);
		}
		kin_en += pow(moment[0][i][0],2) + pow(moment[0][i][1],2);
		coordin[0][i][0] = coordin[1][i][0];
		coordin[0][i][1] = coordin[1][i][1];
		moment[0][i][0] = moment[1][i][0];
		moment[0][i][1] = moment[1][i][1];
	}
	pot_en *= alpha_1;
	pot_en /= num_par;
	kin_en /= 2*m*num_par;
	if( n_iter > start){
		T[0] += kin_en;
	}
	if(a == 0){
		printf("iter = %-6d kin_en/num_par = %-15f pot_en/num_par = %-15f\n", n_iter, kin_en, pot_en);
	}
}


void initial_coord(double coord[amount_of_par][2], const int N, const double dis, double per[]) {
	int  i = 0, j = 0, b = 0;
	int n[2];
	for(i = 1; i < ceil(sqrt(N)+0.001); i++ ){
		if( N%i == 0){
			n[0] = i;
			n[1] = N/i;
		}
	}
	per[0] = dis*(n[0]+1);
	per[1] = dis*sqrt(3)*(n[1]+1)/2;
	for (i = 0; i < n[1]; i++) {
		for (j = i * n[0]; j < (i + 1)*n[0]; j++) {
			if (i % 2 == 0) {
				coord[j][0] = dis * (b + 0.5);
			} else {
				coord[j][0] = dis * (b + 1);
			}
			coord[j][1] = (i + 0.5) * sqrt(3) * dis / 2;
			b++;
		}
		b = 0;
	}
}

void initial_deltX(double coord[amount_of_par][2]) {
	int i = 0, j = 0;
	double ran_val = 0;
	for (i = 0; i < amount_of_par; i++) {
		for (j = 0; j < 2; j++) {
			ran_val = rand() % RAND_MAX + 1;
			ran_val /= RAND_MAX;
			coord[i][j] += distance*(ran_val - 0.5) / 20;
		}
	}
}

void initial_deltP(double momentum[amount_of_par][2], double momentum_0) {
	int i = 0, j = 0;
	double ran_val = 0;
	for (i = 0; i < amount_of_par; i++) {
		for (j = 0; j < 2; j++) {
			ran_val = rand() % RAND_MAX + 1;
			ran_val /= RAND_MAX;
			momentum[i][j] = momentum_0 * (ran_val - 0.5);
		}
	}
}

void print_result_coord_vert(const double coord[amount_of_par][2]) {
	int i = 0, j = 0;
	for (j = 0; j < amount_of_par; j++) {
		printf("%-5d", j);
		for (i = 0; i < 2; i++) {
			printf("%-12f  ", coord[j][i]);
		}
		printf("\n");
	}
	printf("\n");
}



static inline double match_within_R_cut(double r, double R_cut, double l)
{
    if (r < -R_cut)
        return r + l;
    else if (r > R_cut)
        return r - l;
    return r;
}


